from random import randint

from sendmail import sendmail
import config


def zuordnen(anzahl):
    zu_list = []
    for i in range(anzahl):
        found = False
        while not found:
            j = randint(0, anzahl-1)
            if j not in zu_list and (j != i):
                found = True
            elif j not in zu_list and (anzahl - len(zu_list) == 1) and (j == i):
                return zuordnen(anzahl)
        zu_list.append(j)
    return zu_list


if __name__ == '__main__':
    ordnung = zuordnen(len(config.members))
    counter = 0
    password = input("Type your password and press enter: ")
    for i in config.members:
        text = config.mail_text.format(receiver=i['name'], picked_person=config.members[ordnung[counter]]['name'])
        html = config.mail_html.format(receiver=i['name'], picked_person=config.members[ordnung[counter]]['name'])
        counter += 1
        sendmail(config.subject, text, html, i['mail'], password)







