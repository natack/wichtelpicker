import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import config


def sendmail(subject, text, html, receiver, password):
    port = config.mail_server_port  # For SSL
    smtp_server = config.mail_server
    sender_email = config.mail_server_username


    message = MIMEMultipart("alternative")
    message["Subject"] = subject
    message["From"] = config.mail_from
    message["To"] = receiver

    # Turn these into plain/html MIMEText objects
    part1 = MIMEText(text, "plain")
    part2 = MIMEText(html, "html")

    # Add HTML/plain-text parts to MIMEMultipart message
    # The email client will try to render the last part first
    message.attach(part1)
    message.attach(part2)

    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
        server.login(sender_email, password)
        server.sendmail(config.mail_from, receiver, message.as_string())
