# Mail content
mail_text = """\
        Hej {receiver},
        dir wurde {picked_person} zugeordnet.
        (v1)
        """

# Mail content as html
mail_html = """\
        <html>
        <body>
            <p>Hej {receiver},<br>
                dir wurde {picked_person} zugeordnet.<br>
                (v1)
            </p>       
        </body>
        </html>
        """

# List of all members participating
members = [
        {
            'name': 'Max',
            'mail': 'max@example.com'
        },
        {
            'name': 'Moritz',
            'mail': 'moritz@example.com'
        },
     ]

# Mail Server Config
mail_server = "posteo.de"
mail_server_port = 465  # For SSL
mail_server_username = "example@example.com"

# Mail Config
mail_from = "example@example.com"
subject = "Wichteln"
